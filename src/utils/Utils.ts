export class Utils { 

    public static formatDate(timestamp: Date) {
        try {
            var months = ['January','February','March','April',
                'May','June','July','August','September',
                'October','November','December'];
            var year = timestamp.getFullYear();
            var month = months[timestamp.getMonth()];
            var date = timestamp.getDate();
            var hours = timestamp.getHours();
            hours = hours % 12;
            hours = hours ? hours : 12;
            var time = month + ' ' + date + ', ' + year;
            return time;
        } catch(error) {
            return '';
        }
    }

    public static formatInputDate(timestamp: Date) {
        try {
            var months = ['01','02','03','04',
                '05','06','07','08','09',
                '10','11','12'];
            var year = timestamp.getFullYear();
            var month = months[timestamp.getMonth()];
            var date = timestamp.getDate();
            let dateString = date < 10? '0' + date: date;
            var time = year + '-' + month + '-' + dateString;
            return time;
        } catch(error) {
            return '';
        }
    }

}