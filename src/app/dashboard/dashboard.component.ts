import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/DataService'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  data: any = [];
  dateRange: any = [];
  isDataLoaded: boolean = false;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.dataService.get().subscribe(data => {
      let list = data.split('\n');
      let columns = [];
      list.forEach(item => {
        let row = item.split(',');
        if(columns.length === 0) {
          columns = row;
        } else {
          let record = {};
          for(let index = 0; index < columns.length; index++) {
            let value = row[index];
            let column = columns[index];
            if(column === 'date') {
              record[column] = new Date(value + ' 00:00:00');
            } else {
              record[column] = value !== '' ? value : 0;
              if(!isNaN(record[column])) {
                record[column] = parseFloat(record[column]);
              }
            }
          }
          this.data.push(record);
          this.dateRange.push(record['date']);
        }
      });
      this.dateRange = this.dateRange
        .map(function (date: any) { return date.getTime() })
        .filter(function (date: any, i: any, array: any) { return array.indexOf(date) === i; })
        .map(function (time: any) { return new Date(time); });
      this.dateRange.sort(function(a: any, b: any){
        let dateFirst: any = new Date(a);
        let dateSecond: any = new Date(b);
        return dateFirst - dateSecond;
      });
      this.isDataLoaded = true;
    });
  }

}
